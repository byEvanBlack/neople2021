#include "N_Bullet.h"
#include "Engine/Classes/GameFramework/ProjectileMovementComponent.h"


/* 생성자 */
AN_Bullet::AN_Bullet()
{
    // 매 프레임마다 Tick 함수를 실행
    PrimaryActorTick.bCanEverTick = true;

    // 총알 Static Mesh 연결
    Bullet = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BULLET"));   
    RootComponent = Bullet;

    // 충돌 처리 활성화
    Bullet->BodyInstance.bNotifyRigidBodyCollision = false;

    // 움직임 설정을 위한 Projectile Movement Component 생성
    Bullet_movement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("BULLET_movement"));
    Bullet_movement->SetUpdatedComponent(Bullet);
    Bullet_movement->InitialSpeed = 100.0f;
    Bullet_movement->MaxSpeed = 100.0f;
    Bullet_movement->ProjectileGravityScale = 0.0f;
    Bullet_movement->bRotationFollowsVelocity = true;
    Bullet_movement->bShouldBounce = true;
    Bullet_movement->Bounciness = 1.0f;

    // 초기 총알 유지시간
    Death_Count = 99.f;
}


/* 총알 설정 변경 */
void AN_Bullet::Bullet_Setting(FString name, float r, float g, float b, float scale, float life)
{
    // 총알 이름 설정
    NAME = name;

    // 총알 외형 변경
    if (name == "Split")
    {
        UStaticMesh* Bullet_Mesh = LoadObject<UStaticMesh>(nullptr, TEXT("StaticMesh'/Game/Neople_/Bullet_Mesh_Split.Bullet_Mesh_Split'"));
        if (Bullet_Mesh != nullptr)
            Bullet->SetStaticMesh(Bullet_Mesh);
    }
    else
    {
        UStaticMesh* Bullet_Mesh = LoadObject<UStaticMesh>(nullptr, TEXT("StaticMesh'/Game/Neople_/Bullet_Mesh.Bullet_Mesh'"));
        if (Bullet_Mesh != nullptr)
            Bullet->SetStaticMesh(Bullet_Mesh);
    }

    // 총알 방향 및 속도 변경
    Bullet_movement->Velocity = this->GetActorRotation().Vector() * Bullet_movement->InitialSpeed;

    // 총알 색상 변경
    Bullet->SetVectorParameterValueOnMaterials(TEXT("Color"), FVector(r, g, b));

    // 총알 크기 변경
    Bullet->SetWorldScale3D(FVector(scale, scale, scale));

    // 총알 유지시간 변경
    Death_Count = life;
}


/* 총알 유지시간 처리 */
void AN_Bullet::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    // 총알 유지시간이 다 되면 총알을 사라지게 함
    Death_Count -= DeltaTime;
    if (Death_Count <= 0.f)
    {
        // 총알을 삭제함
        Destroy();

        // 분열 발사체인 경우 추가 작업을 진행함
        if (NAME == "Split")
        {
            float radian = this->GetActorRotation().Euler().Z * PI / 180;
            float twist = FMath::Abs(FMath::Sin(radian)) >= FMath::Abs(FMath::Cos(radian)) ? -1.f : 1.f;
            float dist = 25.f;

            AN_Bullet* bullet1 = GetWorld()->SpawnActor<AN_Bullet>(Bullet_actor, FVector(this->GetActorLocation().X + (dist * FMath::Sin(radian)),
                this->GetActorLocation().Y + (dist * FMath::Cos(radian)),
                this->GetActorLocation().Z),
                FRotator(0.f, this->GetActorRotation().Euler().Z + (twist * 45.f), 0.f));
            AN_Bullet* bullet2 = GetWorld()->SpawnActor<AN_Bullet>(Bullet_actor, FVector(this->GetActorLocation().X - (dist * FMath::Sin(radian)),
                this->GetActorLocation().Y - (dist * FMath::Cos(radian)),
                this->GetActorLocation().Z),
                FRotator(0.f, this->GetActorRotation().Euler().Z - (twist * 45.f), 0.f));
            AN_Bullet* bullet3 = GetWorld()->SpawnActor<AN_Bullet>(Bullet_actor, FVector(this->GetActorLocation()), FRotator(this->GetActorRotation()));

            if (bullet1 && bullet2 && bullet3)
            {
                bullet1->Bullet_Setting("Normal", 1.f, 0.f, 0.f, 1.f, 3.f);
                bullet2->Bullet_Setting("Normal", 1.f, 0.f, 0.f, 1.f, 3.f);
                bullet3->Bullet_Setting("Normal", 1.f, 0.f, 0.f, 1.f, 3.f);
            }
        }
    }
}


/* 총알 충돌 처리 */
void AN_Bullet::NotifyHit(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
    if (NAME == "Reflect") {
        // 반사 발사체의 경우 진행방향의 반대방향으로 바꿔줌
        Bullet->SetWorldRotation(this->GetActorRotation() * -1);
    }
    if (NAME != "Reflect") {
        // 그 외에는 충돌 시 총알을 사라지게 함
        Destroy();
    }
}


/* 기타 */
void AN_Bullet::BeginPlay() { Super::BeginPlay(); }