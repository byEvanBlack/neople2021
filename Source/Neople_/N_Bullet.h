#pragma once
#include "EngineMinimal.h"
#include "GameFramework/Actor.h"
#include "N_Bullet.generated.h"


UCLASS()
class NEOPLE__API AN_Bullet : public AActor
{
	GENERATED_BODY()
	
public:	
	/* 생성자 */
	AN_Bullet();

protected:
	virtual void BeginPlay() override;

public:
	// 총알 이름
	FString NAME;

	// 총알 Actor
	TSubclassOf<AN_Bullet> Bullet_actor;

	// 총알 유지시간
	float Death_Count;

	// 총알 Static Mesh
	UPROPERTY(EditAnywhere, Category = "Bullet")
	UStaticMeshComponent* Bullet;

	// 총알 움직임 처리
	UPROPERTY(EditAnywhere, Category = "Movement")
	class UProjectileMovementComponent* Bullet_movement;

	/* 매 프레임마다 실행 */
	/* 총알 유지시간 처리 */
	virtual void Tick(float DeltaTime) override;

	/* 총알 설정 변경 */
	void Bullet_Setting(FString name, float r, float g, float b, float scale, float life);

	/* 총알 충돌 처리 */
	UFUNCTION()
	virtual void NotifyHit(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit);

};
