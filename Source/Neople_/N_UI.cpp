#include "N_UI.h"
#include "Neople_Character.h"
#include "Components/TextBlock.h"
#include "Components/ProgressBar.h"
#include "Components/Button.h"


/* 생성자 */
void UN_UI::NativeConstruct()
{
	Super::NativeConstruct();

	Button_Reset = Cast<UButton>(GetWidgetFromName(TEXT("Button_Reset")));
	Text_Normal = Cast<UTextBlock>(GetWidgetFromName(TEXT("Text_Normal")));
	Text_Charge = Cast<UTextBlock>(GetWidgetFromName(TEXT("Text_Charge")));
	Text_Split = Cast<UTextBlock>(GetWidgetFromName(TEXT("Text_Split")));
	Text_Reflect = Cast<UTextBlock>(GetWidgetFromName(TEXT("Text_Reflect")));
	ProgressBar_Charge = Cast<UProgressBar>(GetWidgetFromName(TEXT("ProgressBar_Charge")));

	ProgressBar_Charge->SetVisibility(ESlateVisibility::Hidden);
	Button_Reset->OnClicked.AddDynamic(this, &UN_UI::Button_Reset_Callback);
}


/* 캐릭터의 델리케이트와 UI 함수를 연동 */
void UN_UI::Bind_CHARA(class ANeople_Character* chara)
{
	if (chara != nullptr)
	{
		// 받아온 캐릭터 포인터에 문제가 없다면 델리케이트와 함수를 연동해줌
		CHARA_CUR = chara;
		chara->BulletChange.BindUObject(this, &UN_UI::Text_Print);
		chara->OnCharge.BindUObject(this, &UN_UI::ProgressBar_Set);
	}
	else
		UE_LOG(LogTemp, Error, TEXT("CHARA ERROR"));
}


/* 초기화 */
void UN_UI::Button_Reset_Callback() 
{
	if (CHARA_CUR.IsValid())
	{
		if (ProgressBar_Charge != nullptr)
		{
			CHARA_CUR->Bullet_Reset();
		}
		else
			UE_LOG(LogTemp, Error, TEXT("Button ERROR"));
	}
	else
		UE_LOG(LogTemp, Error, TEXT("CHARA ERROR"));
}


/* 총알 수 출력 */
void UN_UI::Text_Print()
{
	if (CHARA_CUR.IsValid())
	{
		if (Text_Normal != nullptr && Text_Charge != nullptr && Text_Split != nullptr && Text_Reflect != nullptr)
		{
			Text_Normal->SetText(FText::FromString(FString::FromInt(CHARA_CUR->NORMAL)));
			Text_Charge->SetText(FText::FromString(FString::FromInt(CHARA_CUR->CHARGE)));
			Text_Split->SetText(FText::FromString(FString::FromInt(CHARA_CUR->SPLIT)));
			Text_Reflect->SetText(FText::FromString(FString::FromInt(CHARA_CUR->REFLECT)));
		}
		else
			UE_LOG(LogTemp, Error, TEXT("TextBox ERROR"));
	}
	else
		UE_LOG(LogTemp, Error, TEXT("CHARA ERROR"));
}


/* 총알 수 출력 */
void UN_UI::ProgressBar_Set() 
{
	if (CHARA_CUR.IsValid()) 
	{
		if (ProgressBar_Charge != nullptr) 
		{
			// 차지중일때만 UI가 나타나게 함
			if (CHARA_CUR->Q_Charge)
			{
				ProgressBar_Charge->SetVisibility(ESlateVisibility::Visible);
				ProgressBar_Charge->SetPercent((CHARA_CUR->Q_Delay) / float(3));
			}
			else
			{
				ProgressBar_Charge->SetVisibility(ESlateVisibility::Hidden);
				ProgressBar_Charge->SetPercent(0.f);
			}
		}
		else
			UE_LOG(LogTemp, Error, TEXT("ProgressBar ERROR"));
	}
	else
		UE_LOG(LogTemp, Error, TEXT("CHARA ERROR"));
}