#pragma once
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "N_UI.generated.h"


UCLASS()
class NEOPLE__API UN_UI : public UUserWidget
{
	GENERATED_BODY()
	
public:
	/* 생성자 */
	virtual void NativeConstruct() override;

	/* 캐릭터의 델리케이트와 UI 함수를 연동 */
	void Bind_CHARA(class ANeople_Character* chara);

	// 델리케이트를 위한 캐릭터 포인터
	TWeakObjectPtr<class ANeople_Character> CHARA_CUR;

	// UI 구성요소
	UPROPERTY(EditAnywhere, meta = (BindWidget))
	class UButton* Button_Reset;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	class UTextBlock* Text_Normal;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	class UTextBlock* Text_Charge;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	class UTextBlock* Text_Split;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	class UTextBlock* Text_Reflect;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	class UProgressBar* ProgressBar_Charge;

	/* 초기화 */
	UFUNCTION(BlueprintCallable)
	void Button_Reset_Callback();

	/* 총알 수 출력 */
	UFUNCTION()
	void Text_Print();

	/* 충전 바 출력 */
	UFUNCTION()
	void ProgressBar_Set();
};
