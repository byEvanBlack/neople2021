#include "Neople_Character.h"
#include "N_UI.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"


/* 생성자 */
ANeople_Character::ANeople_Character()
{
	// 매 프레임마다 Tick 함수를 실행
	PrimaryActorTick.bCanEverTick = true;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Create a camera boom attached to the root (capsule)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Rotation of the character should not affect rotation of boom
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->TargetArmLength = 500.f;
	CameraBoom->SocketOffset = FVector(0.f, 0.f, 75.f);
	CameraBoom->SetRelativeRotation(FRotator(0.f, 180.f, 0.f));

	// Create a camera and attach to boom
	SideViewCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SideViewCamera"));
	SideViewCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	SideViewCameraComponent->bUsePawnControlRotation = false; // We don't want the controller rotating the camera

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Face in the direction we are moving..
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 720.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->GravityScale = 2.f;
	GetCharacterMovement()->AirControl = 0.80f;
	GetCharacterMovement()->JumpZVelocity = 1000.f;
	GetCharacterMovement()->GroundFriction = 3.f;
	GetCharacterMovement()->MaxWalkSpeed = 600.f;
	GetCharacterMovement()->MaxFlySpeed = 600.f;

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	// 변수 초기화
	NORMAL = CHARGE = SPLIT = REFLECT = 0;
	Q_Charge = false;

	// UI를 불러와 세팅
	static ConstructorHelpers::FClassFinder<UUserWidget> UI_BP(TEXT("WidgetBlueprint'/Game/Neople_/UI.UI_C'"));
	if (UI_BP.Succeeded())
	{
		UI_Widget = UI_BP.Class;
	}
}


/* 프로그램 시작 전 초기화 */
void ANeople_Character::BeginPlay()
{
	Super::BeginPlay();

	// 불러온 경로에 문제가 없다면 UI를 화면에 띄움
	if (UI_Widget != nullptr)
	{
		CurrentWidget = CreateWidget(GetWorld(), UI_Widget);
		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();
		}
	}

	// 불러온 UI를 토대로 UI와 캐릭터를 Bind
	auto CHARA_UI = Cast<UN_UI>(CurrentWidget);
	if (CHARA_UI != nullptr)
	{
		CHARA_UI->Bind_CHARA(this);
	}
}


/* 움직임 관련 연동 */
void ANeople_Character::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// 기본 움직임
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAxis("Move", this, &ANeople_Character::Move);

	// 스킬
	PlayerInputComponent->BindAction("Skill_Q", IE_Pressed, this, &ANeople_Character::Q_Press);
	PlayerInputComponent->BindAction("Skill_Q", IE_Released, this, &ANeople_Character::Q_Release);
	PlayerInputComponent->BindAction("Skill_W", IE_Pressed, this, &ANeople_Character::W_Press);
	PlayerInputComponent->BindAction("Skill_W", IE_Released, this, &ANeople_Character::W_Release);
}


/* 좌우 이동 */
void ANeople_Character::Move(float Value)
{
	// add movement in that direction
	AddMovementInput(FVector(0.f,-1.f,0.f), Value);
}


/* 매 프레임마다 실행 */
/* 충전 시간 계산 처리 */
void ANeople_Character::Tick(float DeltaTime)
{
	// Q키 누른 시간 측정을 위해 현재 시간 입력
	// 기존에 저장해 둔 시간과의 차이를 비교
	if (Q_Charge)
	{
		Q_End = FDateTime::Now().GetTicks();
		Q_Delay = (float)(Q_End - Q_Begin) / (float)10000000;
		OnCharge.Execute();
	}
}


/* 스킬 입력 */
void ANeople_Character::Q_Press()
{
	// Q키 누른 시간 측정을 위해 현재 시간 입력
	Q_Begin = FDateTime::Now().GetTicks();
	Q_Charge = true;
}

void ANeople_Character::Q_Release()
{
	if (Q_Delay >= 3.0)
	{
		Bullet_Fire("Charge", 0.25f, 0.f, 0.f, 3.f, 5.f);
		CHARGE++;
		BulletChange.Execute();
	}
	if (Q_Delay < 3.0)
	{
		Bullet_Fire("Normal", 1.f, 0.f, 0.f, 1.f, 3.f);
		NORMAL++;
		BulletChange.Execute();
	}

	Q_Charge = false;
	Q_Begin = Q_End = 0;
	Q_Delay = 0.f;
	OnCharge.Execute();
}

void ANeople_Character::W_Press()
{
	if (Q_Delay <= 1.0 && Q_Delay > 0.0)
	{
		Bullet_Fire("Split", 0.f, 1.f, 0.f, 1.f, 3.f);
		SPLIT++;
		BulletChange.Execute();
	}
}

void ANeople_Character::W_Release()
{
	Bullet_Fire("Reflect", 0.f, 0.f, 1.f, 1.f, 5.f);
	REFLECT++;
	BulletChange.Execute();
}


/* 발사를 위해 총알 생성 */
void ANeople_Character::Bullet_Fire(FString name, float r, float g, float b, float scale, float life)
{
	AN_Bullet* bullet = GetWorld()->SpawnActor<AN_Bullet>(Bullet, FVector(Bullet_POS->GetActorLocation()), FRotator(this->GetActorRotation()));
	if (bullet)
	{
		bullet->Bullet_Setting(name, r, g, b, scale, life);
		if (name == "Split") bullet->Bullet_actor = Bullet;
	}
}


/* 총알 카운트 초기화 */
void ANeople_Character::Bullet_Reset()
{
	NORMAL = CHARGE = SPLIT = REFLECT = 0;
	BulletChange.Execute();
}