#pragma once
#include "N_Bullet.h"
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Neople_Character.generated.h"
class UN_UI;


DECLARE_DELEGATE(FBulletChangeDelegate);
DECLARE_DELEGATE(FOnChargeDelegate);


UCLASS(config=Game)
class ANeople_Character : public ACharacter
{
	GENERATED_BODY()

	/** Side view camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* SideViewCameraComponent;

	/** Camera boom positioning the camera beside the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

protected:
	/* 프로그램 시작 전 초기화 */
	virtual void BeginPlay() override;

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface

public:
	/* 생성자 */
	ANeople_Character();

	/** Returns SideViewCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetSideViewCameraComponent() const { return SideViewCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	/* 매 프레임마다 실행 */
	/* 충전 시간 계산 처리 */
	virtual void Tick(float DeltaTime) override;

	/* 좌우 이동 */
	void Move(float Val);

	/* 스킬 입력 */
	UFUNCTION()
	void Q_Press();
	UFUNCTION()
	void Q_Release();
	UFUNCTION()
	void W_Press();
	UFUNCTION()
	void W_Release();

	// 충전 중인지 확인해줄 트리거
	UPROPERTY(EditAnywhere)
	bool Q_Charge;

	// 충전 시간 계산을 위한 변수
	UPROPERTY(EditAnywhere)
	int32 Q_Begin;
	UPROPERTY(EditAnywhere)
	int32 Q_End;
	UPROPERTY(EditAnywhere)
	float Q_Delay;

	/* 발사를 위해 총알 생성 */
	UFUNCTION()
	void Bullet_Fire(FString name, float r, float g, float b, float scale, float life);

	/* 총알 카운트 초기화 */
	UFUNCTION(BlueprintCallable)
	void Bullet_Reset();

	// 발사체 개수
	int32 NORMAL, CHARGE, SPLIT, REFLECT;

	// 총알 Actor
	UPROPERTY(EditAnywhere, Category = "Bullet")
	TSubclassOf<AN_Bullet> Bullet;

	// 총알이 나갈 위치
	UPROPERTY(EditAnywhere, Category = "Bullet Position")
	AActor* Bullet_POS;

	// UI 경로를 받아오고 UI를 출력해 줄 변수
	UPROPERTY()
	TSubclassOf<UUserWidget> UI_Widget;

	UPROPERTY()
	UUserWidget* CurrentWidget;

	// 델리케이트
	FBulletChangeDelegate BulletChange;
	FOnChargeDelegate OnCharge;
};
