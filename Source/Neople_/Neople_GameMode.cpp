#include "Neople_GameMode.h"
#include "Neople_Character.h"
#include "Blueprint/UserWidget.h"
#include "UObject/ConstructorHelpers.h"


/* 생성자 */
ANeople_GameMode::ANeople_GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/SideScrollerCPP/Blueprints/SideScrollerCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// PC를 불러와 세팅
	static ConstructorHelpers::FClassFinder<APlayerController> PC_BP(TEXT("Blueprint'/Game/Neople_/PC.PC_C'"));
	if (PC_BP.Succeeded())
	{
		PlayerControllerClass = PC_BP.Class;
	}
}