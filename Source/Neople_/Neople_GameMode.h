#pragma once
#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Neople_GameMode.generated.h"


UCLASS(minimalapi)
class ANeople_GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	/* 생성자 */
	ANeople_GameMode();

protected:
	// 플레이어 컨트롤러 설정을 위한 변수
	UPROPERTY()
	TSubclassOf<APlayerController> PC;
};



